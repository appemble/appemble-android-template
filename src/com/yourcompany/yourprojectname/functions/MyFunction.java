package com.yourcompany.yourprojectname.functions;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.appemble.avm.actions.CustomFunctionInterface;
import com.appemble.avm.models.ActionModel;

public class MyFunction implements CustomFunctionInterface {

	/**
	 * This function is called as a result of ACTION `FUNCTION`.  
	 * This function return Boolean.valueOf(true) if subsequent peer or nested actions are to be executed. 
	 * By returning Boolean.valueOf(false), the subsequent peer or nested actions will NOT be executed. 
	 *
	 * @param  context - It represents more information about an <a href="http://developer.android.com/reference/android/content/Context.html">Activity"</a> (or screen) on which an event was received and this action is being invoked. 
	 * @param  view - It is a UI component on which an event was received. Good reading about <a href="http://developer.android.com/reference/android/view/View.html">View</a>
	 * @param  parentView - It is the container in which the control is created. The control that is a child of screen, title, CONTROL_GROUP is created in a container of type <a href="http://developer.android.com/reference/android/widget/RelativeLayout.html">Relative Layout</a>. The list item of the LIST control that holds all the children is a relative layout as well. 
	 * @param  action - Defines the attributes of this action "FUNCTION". 
     * @param  targetParameterValueList - The parameter targetParameterValueList is a Bundle. It contains keys defined in the action's attribute input_parameter_list. Note: In case the target needs values with a different name of the key(s), you can define the attribute "target_parameter_list". The target_parameter_list must have the same number of field_name (comma separated) as in the input_parameter_list and MUST be in the same order. 

	 * @author Appemble <a mailto:"support@appemble.com">Support Team</a>
	 * @return Object Boolean.valueOf(true) or Boolean.valueOf(false)
	 */
	@Override
	public Object execute(Context context, View view, View parentView,
		ActionModel action, Bundle targetParameterValueList) {
		
		
		return Boolean.valueOf(true);
	}
}
